import pygame
import random
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from matplotlib import animation
from itertools import combinations
import time 

# Import pygame.locals for easier access to key coordinates
# Updated to conform to flake8 and black standards

from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
)

# define colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GRAY = (128, 128, 128)
LIGHTGRAY = (211, 211, 211)
RED = (255, 0, 0)
CORAL = (255,127,80)
ORANGE = (255,165,0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255,255,0)

SCREEN_WIDTH = 1200
SCREEN_HEIGHT = 800
BACKGROUND = LIGHTGRAY
FPS = 30

class Person (pygame.sprite.Sprite):
    """ This class represents a simple block the player collects. """
 
    def __init__(self):
        """ Constructor, create the image of the block. """
        super().__init__()
        self.image = pygame.Surface([20, 20])
        self.image.fill(BLACK)
        self.rect = self.image.get_rect()
 
    def reset_pos(self):
        """ Called when the block is 'collected' or falls off
            the screen. """
        self.rect.y = random.randrange(SCREEN_HEIGHT)
        self.rect.x = random.randrange(SCREEN_WIDTH)
 
    def update(self):
        """ Automatically called when we need to move the block. """
        self.rect.y += 1
 
        if self.rect.y > SCREEN_HEIGHT + self.rect.height:
            self.reset_pos()

class Simulation(object):
    def __init__(self, npersons):
        self.npersons = npersons
        self.simulation_over = False
        self.person_list = pygame.sprite.Group()
        self.all_sprites_list = pygame.sprite.Group()

        # Create the person sprites
        for i in range(self.npersons):
            person = Person()

            person.rect.x = random.randrange(SCREEN_WIDTH)
            person.rect.y = random.randrange(SCREEN_HEIGHT)

            self.person_list.add(person)
            self.all_sprites_list.add(person)

    def process_events(self):
        for event in pygame.event.get():
            if (event.type == KEYDOWN and event.key==K_ESCAPE) or event.type == pygame.QUIT:
                self.running = False
            else:
                self.running = True
        return self.running

    def run_logic(self):
        if not self.simulation_over:
            self.all_sprites_list.update()

    def display_frame(self, screen):
        """ Display everything to the screen for the simulation. """
        screen.fill(WHITE)
 
        if self.simulation_over:
            # font = pygame.font.Font("Serif", 25)
            font = pygame.font.SysFont("serif", 25)
            text = font.render("Simulation Over, click to restart", True, BLACK)
            center_x = (SCREEN_WIDTH // 2) - (text.get_width() // 2)
            center_y = (SCREEN_HEIGHT // 2) - (text.get_height() // 2)
            screen.blit(text, [center_x, center_y])
 
        if not self.game_over:
            self.all_sprites_list.draw(screen)
 
        pygame.display.flip()
 
if __name__ == "__main__":
    
    pygame.init()
    screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])

    pygame.display.set_caption("Virus spread simulation")
    clock = pygame.time.Clock()

    simulation = Simulation(20)

    start = time.time()
    print('Start!')
    running = True
    while running:
        clock.tick(FPS)
        #for event in pygame.event.get():
        #    if (event.type == KEYDOWN and event.key==K_ESCAPE) or event.type == pygame.QUIT:
        #        running = False

        running = simulation.process_events()
        # Update
        # all_sprites.update()

        # Check to sse if a 

        # Draw / render
        screen.fill(BACKGROUND)

        # Draw a person
        pygame.draw.circle(screen, RED, (250, 250), 5)

        #all_sprites.draw(screen)
        pygame.display.flip()

    pygame.quit()
    end = time.time()
    print('Total Time: ', end-start)

    print ("See you next time!")